##### Description #####

The tool was created to compare input gene clusters to the closest relatives based on user input. It is possible
to provide custom genomes to search against. As search parameters one can either use a hmm or protein fasta file.

##### Download #####

Make sure you have installed and updated:
        - git (check: git --version)
        - anaconda (check: conda --version)

1. Open a terminal and got to the directory where the tool should be downloaded to
2. Enter: git clone https://bitbucket.org/jstahlecker/syn-view.git
3. Go to the new directory: cd syn-view
4. Create the conda envirnoment: conda env create --file environment.yml


##### Usage #####

ACTIVATE the conda environment: conda activate SYN-view
To close the environment simply: conda deactivate

The following inputs must be provided

-i -> input genome(.gbff or .gbk) (annotated)
-search_mode -> either hmm or protein
-file -> either a hmm or fasta file

Choose:
        -mash_distances -> mash_distances.txt file from an autoMLST job (recommended)
        -custom_genomes -> path to the custom genomes. Keep in mind that the genomes must be assembled and annotated

Optional:
-automlst_parselength -> default 10. Specify how many genomes should be downloaded


Example:
You have a genome named genome.gbff and want to find hits based on a hmm called findme.hmm
1. Run autoMLST (https://automlst.ziemertlab.com/) and download the mash_distances.txt file
2. put the file in the same folder as the genome.gbff and the findme.hmm file
3. Open the terminal and go the directory:
        python path/to/syn-view/SYN-view.py -i genome.gbff -search_mode hmm -file findme.hmm -mash_distances mash_distances.txt

You have a genome named genome.gbff and want to find hits based on a protein fasta file called findme.fasta. You have already downloaded all sequences and moved them to a folder called genomes_dir
1. put all files and folder in the same directory
2. Open the terminal and go to the directory:
        python path/to/syn-view/SYN-view.py -i genome.gbff -search_mode protein -file findme.fasta -custom_genomes genomes_dir

You can always provide an absolute path to the input files/folder

