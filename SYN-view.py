# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from arts_parse import *
from pipe2 import *

import time
from itertools import repeat
from multiprocessing import Pool, freeze_support
import subprocess
import os
import sys
import svgutils
sys.path.append(svgutils.__path__)

def check_input(input_gbk, search_mode, custom_genomes, mash_distances, file):
    if input_gbk == "" or input_gbk is None:
        print("Specify input .gbff file")
        sys.exit()

    search_modes = ["HMM", "PROTEIN"]

    try:
        if search_mode.upper() not in search_modes:
            print("Search mode invalid: hmm or protein")
            sys.exit()
    except AttributeError:
        if search_mode is None:
            print("Please specify a search mode")
            sys.exit()

    if file == "":
        print("Specify the search_mode file")
        sys.exit()

    if custom_genomes == "" and mash_distances == "" or (custom_genomes is None and mash_distances is None):
        print("Specify either mash_distances or custom_genomes")
        sys.exit()


def multiprocess_pipe2_blast_other(query_directory, operon_directory, sorting, custom_genomes,
                         search_range):
    query_fasta = [file for file in os.listdir(query_directory) if ("query" in file and ".fasta" in file)][0]

    pipe2_blast_score(operon_directory, query_directory, query_fasta, sorting, custom_genomes,
                      search_range=search_range)
    return "Executed"


def run_automlst_no_folder(result_directory, refdirname, outputdir_automlst):
    """Run automlst."""

    subprocess.run(["mkdir", os.path.join(result_directory, "automlst_res")], check=True)

    inputdir = os.path.join(result_directory, "inputdir/")

    cmd = ["python", "/share/jason/automlst/automlst/automlst.py", "-ref", "/share/jason/automlst/database/refseqreduced.db",
           "-rd", os.path.join("/share/jason/automlst/automlst/", refdirname), "-c", "14", inputdir, outputdir_automlst]
    subprocess.run(cmd, check=True)


def main(input_gbk, result_directory, refdirname, search_mode, file, runautomlst, automlst_parselength,
         search_range, sorting, custom_genomes, mash_distances):


    # if download_faa == 0:
    input_faa = make_faa_file(input_gbk, result_directory)

    result_directory = os.path.abspath(result_directory)

    if mash_distances == "":
        outputdir_automlst = os.path.join(result_directory, "automlst_res")
    else:
        outputdir_automlst = ""

    ##### Create Directory for operon output #####
    subprocess.run(["mkdir", os.path.join(result_directory, "SYN-view_results")], check=True)
    operon_directory = os.path.join(result_directory, "SYN-view_results")
    subprocess.run(["mkdir", os.path.join(operon_directory, "RESULTS")], check=True)

    ##### Run automlst #####
    # put .gbk file into ./antismash/ as xxx_final.gbk
    if runautomlst != 0:
        run_automlst_no_folder(result_directory, refdirname, outputdir_automlst)

    ### Custom Genomes ###
    if custom_genomes != "":
        subprocess.run(["mkdir", os.path.join(operon_directory, "protein_faa")], check=True)
        wd_gbff = custom_genomes
        automlst_file = ""

    else:
        subprocess.run(["mkdir", os.path.join(operon_directory, "protein_faa")], check=True)
        subprocess.run(["mkdir", os.path.join(operon_directory, "genome_gbff")], check=True)
        automlst_file = os.path.join(outputdir_automlst, "mash_distances.txt")

    if mash_distances != "":
        automlst_file = mash_distances

    ### Define Search mode ###
    search_mode = search_mode.upper()

    if search_mode == "HMM":

        ##### make query fastas from hmm_search #####
        hmm_file = file
        output_hmm = "query.hmm"
        outputfile_name = run_hmmsearch(hmm_file, input_faa, os.path.join(operon_directory, output_hmm))
        ids = get_hit_ids(outputfile_name)
        neighbors_list, fois_list, ids_duplicates = neighbors_getter_extended(ids, input_gbk, search_range)
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(fois_list, ids_duplicates)
        query_fasta_maker(reduced_fois_list, input_gbk, operon_directory)
        query_operon_svg_blast(reduced_fois_list, new_ids, operon_directory, search_range)

        ##### Preperation for pipe2 #####
        # Create folders for pipe2

        query_folders = [os.path.join(operon_directory, folder) for folder in os.listdir(operon_directory) if ("query" in folder and os.path.isdir(os.path.join(operon_directory, folder)))]

        pipe2_general(automlst_file, hmm_file, automlst_parselength, operon_directory, search_range, download_faa=0,
                      custom_genomes=custom_genomes)

        #start_time = time.time()
        #print("starting Loops")

        # Multithreaded sorting
        with Pool() as pool:
            pool.starmap(multiprocess_pipe2_blast_other, zip(query_folders, repeat(operon_directory), repeat(sorting),
                                                             repeat(custom_genomes), repeat(search_range)))


        #print("Finished in {} seconds".format(time.time()-start_time))

    if search_mode == "PROTEIN":
        fasta_file = file
        make_db(input_faa, "query_db", operon_directory)
        query_blast_output = os.path.join(operon_directory, ".".join(input_faa.split(".")[:-1]) + "_blast_res.txt")
        run_local_blast_fmt6(fasta_file, os.path.join(operon_directory, input_faa), query_blast_output, e_value=1e-20)
        ids = blast_hit_ids(query_blast_output)
        neighbors_list, fois_list, ids_duplicates = neighbors_getter_extended(ids, input_gbk, search_range)
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(fois_list, ids_duplicates)
        query_fasta_maker(reduced_fois_list, input_gbk, operon_directory)
        query_operon_svg_blast(reduced_fois_list, new_ids, operon_directory)

        query_folders = [os.path.join(operon_directory, folder) for folder in os.listdir(operon_directory) if ("query" in folder and os.path.isdir(os.path.join(operon_directory, folder)))]

        pipe2_general_blast(automlst_file, fasta_file, automlst_parselength, operon_directory, search_range,
                            download_faa=0, custom_genomes=custom_genomes)

        #start_time = time.time()
        #print("starting Loops")

        with Pool() as pool:
            pool.starmap(multiprocess_pipe2_blast_other, zip(query_folders, repeat(operon_directory), repeat(sorting),
                                                             repeat(custom_genomes), repeat(search_range)))


        #print("Finished in {} seconds".format(time.time()-start_time))

    # Move results to results folder
    RESULT_SVG = "operon_hits_query"
    RESULT_TXT = "operon_hits_query"
    for query_folder in query_folders:
        for file in os.listdir(query_folder):
            if RESULT_SVG in file:
                subprocess.run("cp -a {} {}".format(os.path.join(query_folder, file), os.path.join(result_directory, "SYN-view_results" ,"RESULTS")), check=True, shell=True )
            if RESULT_TXT in file:
                subprocess.run("cp -a {} {}".format(os.path.join(query_folder, file), os.path.join(result_directory,"SYN-view_results", "RESULTS")), check=True, shell=True)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # Obligatory
    parser.add_argument("-i", "--input_gbk", help="Input gbk file", action="store")
    parser.add_argument("-search_mode", help="Specify search mode: hmm, Protein", action="store")
    parser.add_argument("-file", help="Either hmm file or .fasta file", action="store")

    # Choose one
    parser.add_argument("-mash_distances", help="Specify mash_distances.txt file", nargs="?", default="",
                        action="store")
    parser.add_argument("-custom_genomes", help="Specify custom genomes to search against", nargs="?", default="",
                        action="store")
    # Optional
    parser.add_argument("-search_range", nargs="?", default=3, type=int, action="store")
    parser.add_argument("-automlst_parselength", nargs="?", default=10, type=int, action="store")
    parser.add_argument("-rd", "--resultdir", help="Specify result direcotry", nargs="?", default=".", type=str,
                        action="store")


    # Probably remove
    parser.add_argument("-runautomlst", help="Should automlst be run?", nargs="?", type=int, default=0 ,action="store")
    parser.add_argument("-sorting", help= "Define sorting: 0 -> sort with position weighting\n 1 -> use bit score",
                        nargs="?", default=1, type=int, action="store")
    parser.add_argument("-ref", "--refdir", help="Name of the reference folder (automlst)", nargs="?", default="",
                        action="store")

    args = parser.parse_args()
    print(args.custom_genomes, args.mash_distances)
    freeze_support() # In case running on Win
    check_input(args.input_gbk, args.search_mode, args.custom_genomes, args.mash_distances, args.file)

    # Check if SYN-view_results already exists
    if "SYN-view_results" in os.listdir(args.resultdir):
        print("Please rename or remove the folder: 'SYN-view_results' and rerun the script!")
        sys.exit()

    # Check if .faa file already exists
    check_name = "_".join(os.path.basename(args.input_gbk).split("_")[0:2]) + "_protein.faa"
    if check_name in os.listdir(args.resultdir):
        print("Please rename or remove the .faa of the input .gbk file and rerun the script!")
        sys.exit()

    main(args.input_gbk, args.resultdir, args.refdir, args.search_mode, args.file, args.runautomlst,
         args.automlst_parselength, args.search_range, args.sorting, args.custom_genomes,
         args.mash_distances)