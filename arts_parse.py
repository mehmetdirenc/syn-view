# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import json
from Bio import SeqIO
import argparse
from seq_features_to_fasta import feature_record_converter
import os
import sys
import subprocess


def neighbors_getter_location(location_list, gb_file, search_range=3):
    """Find the 3 neighboring proteins of the ids of the hmm result."""
    gb_records = SeqIO.parse(gb_file, "genbank")

    # create a list per record with only cds features.
    cds_only_records = []
    for record in gb_records:
        cds_only_records.append([feature for feature in record.features if feature.type == "CDS"])

    # list with tuples of all features of interest (FOI) (index_record_list, index_feature, protein_id)
    indices_of_FOI = []
    for loc in location_list:
        start = int(loc[0])
        end = int(loc[1])
        for index_record_list, record_list in enumerate(cds_only_records):
            for index_feature, feature in enumerate(record_list):
              if start == int(feature.location.start):
                  indices_of_FOI.append((index_record_list, index_feature, feature.qualifiers["protein_id"][0]))


    # search for neighboring as defined by search_range and append to new list.
    fois_search = []
    as_features = []
    for foi in indices_of_FOI:
        rec_list = cds_only_records[foi[0]]
        feat_ind = foi[1]
        feature_list = []
        inner_features = []
        for i in list(range(-search_range, search_range + 1)):
            try:
                new_ind = feat_ind + i
                feature_list.append(rec_list[new_ind].qualifiers["product"][0])
                inner_features.append(rec_list[new_ind])
            except IndexError:
                continue
        fois_search.append(feature_list)
        as_features.append(inner_features)
    return fois_search, as_features


def arts_parser(hmm_model, coretable_json):
    """Parses though arts coretable.json file and searches for hits with the given hmm Name"""
    with open(coretable_json) as f:
        coretable_dict = json.load(f)

    coretable_data = coretable_dict["data"]
    allhits_list = False
    for hit_dict in coretable_data:
        if hit_dict["coregene"] == hmm_model:
            allhits_list = hit_dict["allhits"][1:-2].split(";")

    if not allhits_list:
        print("No hits found for the model")
        sys.exit()

    else:
        locations = []
        for hit in allhits_list:
            loc_index = hit.split("|").index("loc")
            location_list = hit.split("|")[loc_index + 1].split(" ")[0:2]
            locations.append((location_list[0], location_list[1]))
        return locations



def query_fasta_maker(operon_features, gbk_file, outputdir="./"):
    """Takes the features and creates a query fasta. Operon no. may not be preserved, therefore 'X' will be added!
    Check results for self hits!"""
    basename = "_".join(os.path.basename(gbk_file).split("_")[0:2])
    counter = -1
    for index_operon, operon in enumerate(operon_features):
        if len(operon) > 3:
            counter += 1
            query_path = os.path.join(outputdir, "query" + str(counter))
            subprocess.run(["mkdir", query_path])
            with open(os.path.join(query_path, "query" + str(counter) + ".fasta"), "w") as f:
                for index_column, feature in enumerate(operon):
                    try:
                        record = feature_record_converter(feature, basename, "X", index_column)
                        SeqIO.write(record, f, "fasta")
                    except KeyError:
                        pass





def main(hmm_model, coretable_json, gbk_file, searchrange):
    location_list = arts_parser(hmm_model, coretable_json)
    fois_search, as_features = neighbors_getter_location(location_list, gbk_file, searchrange)
    query_fasta_maker(as_features, gbk_file)
    print(fois_search)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("hmm_model", help="TIGRFAM model name", type=str, action="store")
    parser.add_argument("coretable_json", help="coretable.json", action="store")
    parser.add_argument("gbk_file", help="Input genome in genbank (from antismash)", action="store")
    parser.add_argument("-search_range", nargs="?", default=3, type=int)

    args = parser.parse_args()

    main(args.hmm_model, args.coretable_json, args.gbk_file, args.search_range)