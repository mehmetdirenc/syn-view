# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import csv
import itertools
import subprocess
from search_ncbi import search_for_genome
from neighbor_searcher2 import run_hmmsearch, get_hit_ids, neighbors_getter_extended


def automlst_parser(automlst_file, parselength=50):
    """Parses through automlst result and returns list of tuple (Reference_assembly, Estimated_ANI)"""
    PARSE_LENGTH = parselength
    PARSE_START = 1
    PARSE_END = PARSE_START + PARSE_LENGTH
    with open(automlst_file, "r") as f:
        read_tsv = csv.reader(f, delimiter="\t")
        automlst_res = [(row[1], row[4]) for row in itertools.islice(read_tsv, PARSE_START, PARSE_END)]

    return automlst_res

def download_and_unzip(automlst_res, wd_faa, wd_gbff, outputdir, download_faa=1):

    for result in automlst_res:
        search_for_genome(result[0], outputdir, download_faa=download_faa)

    # unzip all files
    if download_faa == 1:
        subprocess.run("gunzip *", shell=True, cwd=wd_faa ,check=True)
    subprocess.run("gunzip *", shell=True, cwd=wd_gbff, check=True)



