# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with ARTS
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from arts_parse import *
from pipe2 import *

import subprocess
import os
import sys
import svgutils
sys.path.append(svgutils.__path__)


def run_automlst_no_folder(result_directory, refdirname, outputdir_automlst):
    """Run automlst."""

    subprocess.run(["mkdir", os.path.join(result_directory, "automlst_res")], check=True)

    inputdir = os.path.join(result_directory, "inputdir/")

    cmd = ["python", "/share/jason/automlst/automlst/automlst.py", "-ref", "/share/jason/automlst/database/refseqreduced.db",
           "-rd", os.path.join("/share/jason/automlst/automlst/", refdirname), "-c", "14", inputdir, outputdir_automlst]
    subprocess.run(cmd, check=True)

def main(input_gbk, input_faa, result_directory, refdirname, hmm_model, runautomlst, automlst_parselength, search_range, sorting):

    # hmm_model = hmm_model.upper()
    result_directory = os.path.abspath(result_directory)
    outputdir_automlst = os.path.join(result_directory, "automlst_res")

    ##### Create Directory for operon output #####
    subprocess.run(["mkdir", os.path.join(result_directory, "SYN-view_results")], check=True)
    operon_directory = os.path.join(result_directory, "SYN-view_results")

    ##### Run automlst #####
    # put .gbk file into ./antismash/ as xxx_final.gbk
    if runautomlst != 0:
        run_automlst_no_folder(result_directory, refdirname, outputdir_automlst)

    ##### make query fastas from hmm_search #####
    # hmm_file = os.path.join("/share/jason/hmm_models/TIGRFAMs_15.0_HMM/", hmm_model + ".HMM")
    hmm_file = hmm_model
    output_hmm = "query.hmm"
    outputfile_name = run_hmmsearch(hmm_file, input_faa, os.path.join(operon_directory, output_hmm))
    ids = get_hit_ids(outputfile_name)
    neighbors_list, fois_list, ids_duplicates = neighbors_getter_extended(ids, input_gbk, search_range)
    query_fasta_maker(fois_list, input_gbk, operon_directory)
    query_operon_svg_blast(fois_list, operon_directory)


    ##### Preperation for pipe2 #####
    # Create folders for pipe2
    subprocess.run(["mkdir", os.path.join(operon_directory, "protein_faa")], check=True)
    subprocess.run(["mkdir", os.path.join(operon_directory, "genome_gbff")], check=True)
    automlst_file = os.path.join(outputdir_automlst, "mash_distances.txt")
    # get hmm file
    # query_fastas = [file for file in os.listdir(operon_directory) if ("query" in file and ".fasta" in file)] # define the querys
    # query_fasta = os.path.join(result_directory, operon_dir, "query0", "query0.fasta")
    # # FOR NOW ONLY TAKE THE FIRST QUERY
    # # query_fasta = query_fastas[0]
    #
    # ##### Actual pipeline #####
    # pipe2(automlst_file, hmm_file, automlst_parselength, query_fasta, operon_directory, search_range, sorting)

    query_folders = [os.path.join(operon_directory, folder) for folder in os.listdir(operon_directory) if ("query" in folder and os.path.isdir(os.path.join(operon_directory, folder)))]

    pipe2_general(automlst_file, hmm_file, automlst_parselength, operon_directory, search_range)

    for query_directory in query_folders:
        query_fasta = [file for file in os.listdir(query_directory) if ("query" in file and ".fasta" in file)][0]
        pipe2_blast_score(operon_directory, query_directory, query_fasta, sorting)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_gbk", help="Input gbk file", action="store")
    parser.add_argument("-ifaa", "--input_faa", help="input faa file", action="store")
    parser.add_argument("-rd", "--resultdir", help="Specify result direcotry", action="store")
    parser.add_argument("-ref", "--refdir", help="Name of the reference folder (automlst)", action="store")
    parser.add_argument("-hmm", help="hmm file name", action="store")

    parser.add_argument("-runautomlst", help="Should automlst be run?", nargs="?", type=int, default=1 ,action="store")
    parser.add_argument("-automlst_parselength", nargs="?", default=50, type=int)
    parser.add_argument("-search_range", nargs="?", default=3, type=int)
    parser.add_argument("-sorting", help= "Define sorting: 0 -> sort with position weighting\n 1 -> use bit score", nargs="?", default=1, type=int)


    args = parser.parse_args()

    main(args.input_gbk, args.input_faa, args.resultdir, args.refdir, args.hmm, args.runautomlst, args.automlst_parselength, args.search_range, args.sorting)