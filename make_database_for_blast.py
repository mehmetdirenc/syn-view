# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import subprocess
from Bio import SeqIO
from Bio.Blast import NCBIXML
import os



def make_db(fasta_file, db_name, outputdir):
    """Create a database based on input fasta file.
    input: path to fasta file"""
    print("Creating Blastp Database for file {}".format(fasta_file))
    fasta_basename = os.path.basename(fasta_file)
    cmd = ["makeblastdb", "-in", fasta_file, "-dbtype", "prot", "-out", os.path.join(outputdir, fasta_basename)]
    subprocess.run(cmd, check=True, stdout=subprocess.DEVNULL)

def run_local_blast(query, db, output, format=5):
    """run blastp search against custom databse."""
    cmd = ["blastp", "-db", db, "-query", query, "-out", output, "-outfmt", str(format)]
    subprocess.run(cmd, check=True)

def bonus_check(query_name, target_name, i=0.5):
    """Checks if the hit is in the same column and gives extra point if"""
    SAME_POSITION_BONUS = i
    score = 0
    query_column = query_name.split("|")[2]
    target_column = target_name.split("|")[2]
    if target_column == query_column:
        score = SAME_POSITION_BONUS
    return score


def blast_scorer_sorter(filename, output_filename, E_VALUE_THRESH=0.04):
    """Takes blast record and scores the result to the query. Scores are defined as Hit + 0.5 * Same column."""
    with open(filename) as f:
        blast_records = list(NCBIXML.parse(f))

    total_score = {}
    E_VALUE_THRESH = E_VALUE_THRESH

    for record in blast_records: # loop through records
        query_name = record.query.split(" ")[0]
        query_operon_id = "|".join(query_name.split("|")[0:2])
        local_score = {}
        for alignment in record.alignments: # loop through alignments (=hits)
            hsp = alignment.hsps[0] # first high-scoring pair (dont need actual alignment, same id anyways)
            alignment_name = alignment.title.split(" ")[1]
            alignment_operon = "|".join(alignment.title.split(" ")[1].split("|")[0:2])
            if (query_operon_id != alignment_operon) and (hsp.expect < E_VALUE_THRESH): # don't score to same operon
                if not alignment_operon in local_score: # create dictionary entry
                    local_score[alignment_operon] = 1 # There obviously was a hit
                # Do the scoring
                local_score[alignment_operon] += bonus_check(query_name, alignment_name) # add bonus if in the same column
            # add score to total_score
        total_score = {key: total_score.get(key, 0) + local_score.get(key, 0)
                  for key in set(total_score) | set(local_score)} # Update total_score for each hit in the operon


    # sort dict as tuple
    sorted_score = sorted(total_score.items(), key=lambda x: x[1], reverse=True)
    # top_scores = sorted_score[0:5] # intially get top 5
    #
    # last_hit = top_scores[-1]
    # for score in sorted_score: # Check if there are more hits with same score and append if not already in list
    #     if (score not in top_scores) and (last_hit[1] == score[1]):
    #         top_scores.append(score)
    #
    # with open(output_filename, "w") as f:
    #     DELIMITER = "\t"
    #     for score in top_scores:
    #         f.write(score[0] + DELIMITER + str(score[1]) + "\n")

    # use all scores
    with open(output_filename, "w") as f:
        DELIMITER = "\t"
        for score in sorted_score:
            f.write(score[0] + DELIMITER + str(score[1]) + "\n")

# Increase threshold? -> 1e-20? Remove sorting 0?
def blast_scorer_sorter2(filename, output_filename, E_VALUE_THRESH=1e-20, sorting=1):
    """Takes blast record and scores the result to the query. Scores are defined as Hit + 0.5 * Same column. Or by
    blast bit score."""
    with open(filename) as f:
        blast_records = list(NCBIXML.parse(f))

    total_score = {}
    E_VALUE_THRESH = E_VALUE_THRESH

    if sorting == 0: # sort with weighting to location

        for record in blast_records: # loop through records
            query_name = record.query.split(" ")[0]
            query_operon_id = "|".join(query_name.split("|")[0:2])
            query_assembly = record.query.split(" ")[0].split("|")[0]
            query_start = record.query.split(" ")[2].split("...")[0] # result is string in case "<" or ">" is in name
            query_end = record.query.split(" ")[2].split("...")[1]
            local_score = {}
            for alignment in record.alignments: # loop through alignments (=hits)
                hsp = alignment.hsps[0] # first high-scoring pair (dont need actual alignment, same id anyways)
                alignment_name = alignment.title.split(" ")[1]
                alignment_assembly = alignment.title.split(" ")[1].split("|")[0]
                alignment_operon = "|".join(alignment.title.split(" ")[1].split("|")[0:2])
                alignment_start = alignment.title.split(" ")[3].split("...")[0]
                alignment_end = alignment.title.split(" ")[3].split("...")[1]

                if (query_assembly != alignment_assembly) and (hsp.expect < E_VALUE_THRESH): # don't score to same assembly
                    if not alignment_operon in local_score: # create dictionary entry
                        local_score[alignment_operon] = 1 # There obviously was a hit
                    # Do the scoring
                    local_score[alignment_operon] += bonus_check(query_name, alignment_name) # add bonus if in the same column

                elif (query_assembly == alignment_assembly) and (hsp.expect < E_VALUE_THRESH): # add other operons from same assembly
                    if (query_start != alignment_start) and (query_end != alignment_end):
                        if not alignment_operon in local_score:  # create dictionary entry
                            local_score[alignment_operon] = 1  # There obviously was a hit
                        # Do the scoring
                        local_score[alignment_operon] += bonus_check(query_name,
                                                                     alignment_name)  # add bonus if in the same column
                # add score to total_score
            total_score = {key: total_score.get(key, 0) + local_score.get(key, 0)
                      for key in set(total_score) | set(local_score)} # Update total_score for each hit in the operon

    if sorting == 1: # cummulative blast score
        for record in blast_records:  # loop through records
            query_name = record.query.split(" ")[0]
            query_operon_id = "|".join(query_name.split("|")[0:2])
            query_assembly = record.query.split(" ")[0].split("|")[0]
            query_start = record.query.split(" ")[2].split("...")[0] # result is string in case "<" or ">" is in name
            query_end = record.query.split(" ")[2].split("...")[1]
            local_score = {}
            for alignment in record.alignments:  # loop through alignments (=hits)
                for hsp in alignment.hsps: # loop through hsps, since top score is of interest.
                    # alignment_name = alignment.title.split(" ")[1]
                    alignment_operon = "|".join(alignment.title.split(" ")[1].split("|")[0:2])
                    alignment_assembly = alignment.title.split(" ")[1].split("|")[0]
                    alignment_start = alignment.title.split(" ")[3].split("...")[0]
                    alignment_end = alignment.title.split(" ")[3].split("...")[1]

                    if (query_assembly != alignment_assembly) and (hsp.expect < E_VALUE_THRESH):  # don't score to same assembly
                        if not alignment_operon in local_score:  # create dictionary entry
                            local_score[alignment_operon] = hsp.bits  # assign to bit score
                        else:
                            if local_score[alignment_operon] < hsp.bits: # if there was a better hit take it (regardless of position)
                                local_score[alignment_operon] = hsp.bits
                            else:
                                pass

                    elif (query_assembly == alignment_assembly) and (hsp.expect < E_VALUE_THRESH):  # add other operons from same assembly
                        if (query_start != alignment_start) and (query_end != alignment_end):
                            if not alignment_operon in local_score:  # create dictionary entry
                                local_score[alignment_operon] = hsp.bits  # assign to bit score
                                # print("Creating Entry", alignment_operon, hsp.bits, query_start, alignment_start, query_end, alignment_end, record.query, alignment.title)
                            else:
                                if local_score[
                                    alignment_operon] < hsp.bits:  # if there was a better hit take it (regardless of position)
                                    local_score[alignment_operon] = hsp.bits
                                else:
                                    pass

                # add score to total_score
            total_score = {key: total_score.get(key, 0) + local_score.get(key, 0)
                           for key in
                           set(total_score) | set(local_score)}  # Update total_score for each hit in the operon


    # sort dict as tuple
    sorted_score = sorted(total_score.items(), key=lambda x: x[1], reverse=True)
    # top_scores = sorted_score[0:5] # intially get top 5
    #
    # last_hit = top_scores[-1]
    # for score in sorted_score: # Check if there are more hits with same score and append if not already in list
    #     if (score not in top_scores) and (last_hit[1] == score[1]):
    #         top_scores.append(score)
    #
    # with open(output_filename, "w") as f:
    #     DELIMITER = "\t"
    #     for score in top_scores:
    #         f.write(score[0] + DELIMITER + str(score[1]) + "\n")

    # Use all scores
    with open(output_filename, "w") as f:
        DELIMITER = "\t"
        for score in sorted_score:
            f.write(score[0] + DELIMITER + str(score[1]) + "\n")


