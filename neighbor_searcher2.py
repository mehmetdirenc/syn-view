# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import subprocess
import argparse
from Bio import SeqIO
from Bio import SearchIO

def run_hmmsearch(hmm_file, seqdb, outputfile, e_vaule=1e-20):
    """Run hmmsearch."""
    print("Running hmmsearch for file {}".format(outputfile))
    cmd = ["hmmsearch", "-E", str(e_vaule), "--tblout",  outputfile, hmm_file, seqdb]
    # cmd = ["/opt/anaconda3/bin/hmmsearch","--tblout", outputfile, hmm_file, seqdb]
    subprocess.run(cmd, check=True, stdout=subprocess.DEVNULL)
    return str(outputfile)


def get_hit_ids(outputfilename):
    """Parses through hmmsearch output file and returns list of target_names"""
    for result in SearchIO.parse(outputfilename, "hmmer3-tab"):
        hits = result.hits
    ids = [hit.id for hit in hits]

    return ids


# find neighboring cds

def neighbors_getter_extended(id_list, gb_file, search_range=3):
    """Find the 3 neighboring proteins of the ids of the hmm result. Returns named features, features as features, and
    an updated id list (accounts for multiple hits)."""

    gb_records = list(SeqIO.parse(gb_file, "genbank"))

    # create a list per record with only cds features.
    cds_only_records = []
    for record in gb_records:
        cds_only_records.append([feature for feature in record.features if feature.type == "CDS"])

    # list with tuples of all features of interest (FOI) (index_record_list, index_feature, protein_id)
    ids_with_duplicates = [] # If protein_id is more than once in genome fois_list is longer than ids --> ERROR
    indices_of_FOI = []
    for id in id_list:
        for index_record_list, record_list in enumerate(cds_only_records):
            for index_feature, feature in enumerate(record_list):
                try:
                    if id == feature.qualifiers["protein_id"][0]:
                        indices_of_FOI.append((index_record_list, index_feature, feature.qualifiers["protein_id"][0]))
                        ids_with_duplicates.append(id)
                except KeyError:
                    # print("Feature found without protein_id")
                    pass

    # search for neighboring as defined by search_range and append to new list.
    fois_search = []
    as_features = []
    for foi in indices_of_FOI:
        rec_list = cds_only_records[foi[0]]
        feat_ind = foi[1]
        feature_list = []
        inner_features = []
        for i in list(range(-search_range, search_range + 1)):
            try:
                new_ind = feat_ind + i
                if new_ind < 0:
                    continue
                feature_list.append(rec_list[new_ind].qualifiers["product"][0])
                inner_features.append(rec_list[new_ind])
            except IndexError:
                continue
        fois_search.append(feature_list)
        as_features.append(inner_features)
    return fois_search, as_features, ids_with_duplicates




def main(hmm_file, faa_file, gb_file, output_hmm, output_neighbors, search_range):

    outputfile_name = run_hmmsearch(hmm_file, faa_file, output_hmm)
    ids = get_hit_ids(outputfile_name)
    neighbors_list = neighbors_getter_extended(ids, gb_file, search_range)

    with open(output_neighbors, "w") as f:
        DELIMITER = "\t"
        for index, operon in enumerate(neighbors_list):
            f.write("Based on hit {}\n".format(ids[index]))
            for protein in operon:
                f.write(str(DELIMITER + protein + "\n"))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("hmm_file", help="TIGRFAMs .HMM file", action= "store")
    parser.add_argument("faa_file", help=".faa file containing all proteins", action= "store")
    parser.add_argument("gb_file", help="Please provide a full genome file in genbank format", action="store")
    parser.add_argument("output_hmm", help= "specify outputfilename for hmmsearch", action= "store")
    parser.add_argument("outputfile", help = "outputfile for operon view", action= "store")
    parser.add_argument("-search_range", nargs="?", default=3, type=int)

    args = parser.parse_args()

    main(args.hmm_file, args.faa_file, args.gb_file, args.output_hmm, args.outputfile, args.search_range)