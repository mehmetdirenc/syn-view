# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from Bio.Graphics.GenomeDiagram import *
from reportlab.lib import colors
from Bio.Blast import NCBIXML
from Bio.SeqFeature import SeqFeature, FeatureLocation
from seq_features_to_fasta import *
import csv
import svgutils
import os
import sys
from neighbor_searcher2 import *


def color_getter_blastres(blast_file, assembly_name, operon_no, position_no):
    """Define colors based on blast res."""
    E_VALUE_TRESH = 1e-20
    with open(blast_file) as f:
        blast_records = list(NCBIXML.parse(f)) # open this earlier to save time

    to_search = assembly_name + "|" + str(operon_no) + "|" + str(position_no)
    best_match = []

    for record in blast_records:
        query_operon = record.query.split(" ")[0].split("|")[2]
        for alignment in record.alignments:
            if to_search == alignment.title.split(" ")[1]:
                for hsp in alignment.hsps:
                    if hsp.expect < E_VALUE_TRESH:
                        best_match.append((int(query_operon), hsp.identities))

    best_match.sort(key= lambda x: x[1], reverse=True)

    color_list = [colors.gray,  colors.darkred, colors.purple, colors.cyan,
                  colors.magenta, colors.orange, colors.greenyellow, colors.darkgreen,
                  colors.aquamarine, colors.beige, colors.brown, colors.chartreuse, colors.chocolate,
                  colors.darksalmon, colors.sandybrown, colors.hotpink, colors.bisque, colors.darkmagenta,
                  colors.gold, colors.limegreen, colors.peru,colors.white]

    # print(best_match)
    if len(best_match) == 0:
        return color_list[-1] # if there was no match return white
    else:
        return color_list[best_match[0][0]] # retrun color based on query operon


def color_getter(feature):
    """Get color for feature based on product."""
    groups = ["hypothetical", "Clp", "regulator", "factor", "ase"]
    name = feature.qualifiers["product"][0]
    id = -1
    for group in groups:
        if group in name:
            id = groups.index(group)
            break
    color_list = [colors.gray,  colors.darkred, colors.purple, colors.cyan,
                  colors.magenta, colors.orange, colors.black, colors.greenyellow, colors.darkgreen]
    if id in range(len(groups)):
        color = color_list[id]
    else:
        color = colors.black
    return color

def length_checker(feature):
    if len(feature.qualifiers["product"][0]) > 30:
        return "..."
    else:
        return ""


def svg_stacker(svgs, outputdir):
    SCALE = 0.5
    BASE_WIDTH =  1200 # 357 / 0.3
    BASE_HEIGHT = 843 * SCALE
    # OFFSET = 175 / 0.3
    OFFSET = 200
    SMALL_OFFSET = 200 * SCALE
    opened = [svgutils.transform.fromfile(x).getroot() for x in svgs]
    figure = svgutils.transform.SVGFigure(str(BASE_WIDTH) + "px", str(BASE_HEIGHT* len(svgs)) + "px")
    for index, fig in enumerate(opened):
        if index == 0:
            fig.moveto(0,0, scale= 0.5)
        # if index == 1:
        #     fig.moveto(0, OFFSET, scale=SCALE) # scale=0.3
        elif index >= 1:
            fig.moveto(0, index * SMALL_OFFSET + OFFSET, scale= SCALE)
    figure.append(opened)
    figure.save(os.path.join(outputdir, "operon_hits_{}.svg".format(os.path.basename(outputdir))))


def query_operon_svg_blast(as_features, ids, outputdir, search_range=3):

    color_list = [colors.gray,  colors.darkred, colors.purple, colors.cyan,
                  colors.magenta, colors.orange, colors.greenyellow, colors.darkgreen,
                  colors.aquamarine, colors.beige, colors.brown, colors.chartreuse, colors.chocolate,
                  colors.darksalmon, colors.sandybrown, colors.hotpink, colors.bisque, colors.darkmagenta,
                  colors.gold, colors.limegreen, colors.peru,colors.white]

    ## Get strand of id ##
    strand_list = []
    long_as_features = []
    for index_ids, operon in enumerate(as_features):
        if len(operon) > 3:
            long_as_features.append(operon)
            id = ids[index_ids]
            try:
                if id == operon[search_range].qualifiers["protein_id"][0]:
                    strand_list.append(operon[search_range].location.strand)
                else:
                    for feature_index, feature in enumerate(operon):
                        try:
                            if id == feature.qualifiers["protein_id"][0]:
                                strand_list.append(feature.location.strand)
                        except KeyError:
                            pass
            except KeyError:
                for feature_index, feature in enumerate(operon):
                    try:
                        if id == feature.qualifiers["protein_id"][0]:
                            strand_list.append(feature.location.strand)
                    except KeyError:
                        pass
        else:
            print("Gene Cluster too small, therefore the hit was ignored.")

    if len(long_as_features) == 0:
        print("All hits are too small, please check the input file!")
        sys.exit()
    reversed_as_features = []
    for i in range(len(long_as_features)): # change each operon individually
        strand_foi = strand_list[i] # check strand of operon
        if strand_foi == -1:
            operon_start = long_as_features[i][0].location.start
            operon_end = long_as_features[i][-1].location.end
            # reversed_as_features = []

            reversed_operon = []
            for feature in long_as_features[i]:
                reversed_operon.append(
                    SeqFeature(FeatureLocation(-int(feature.location.end) + operon_end,
                                               -int(feature.location.start) + operon_end,
                                               strand=-feature.location.strand), type="CDS",
                               qualifiers=feature.qualifiers))
            reversed_operon.reverse()
            reversed_as_features.append(reversed_operon)

        else:
            reversed_as_features.append(long_as_features[i])



    for index, operon in enumerate(reversed_as_features):
        query_path = os.path.join(outputdir, "query" + str(index))
        gd_diagram = Diagram("Test Diagram")
        gd_track = gd_diagram.new_track(1, name= "Query" + str(index) + "    " +
                                                 str(as_features[index][0].location.start) + "..." + str(as_features[index][-1].location.end),
                                        scale_ticks=0,
                                        greytrack=1, greytrack_labels=1, greytrack_fontsize=25)
        gd_feature_set = gd_track.new_set()
        for feature_index, feature in enumerate(operon):

            if long_as_features[index] != reversed_as_features[index]:
                old_pos_no = len(long_as_features[index]) - 1 - feature_index
            else:
                old_pos_no = feature_index

            try:
                has_translation = feature.qualifiers["translation"] # tests to see if feature has a translation
                gd_feature_set.add_feature(feature, color=color_list[old_pos_no], label=True, label_size=25,
                                           sigil="BIGARROW",
                                           name=feature.qualifiers["product"][0][0:30] + length_checker(feature),
                                           arrowhead_length=0.25, label_position="middle")
            except KeyError:
                gd_feature_set.add_feature(feature, color=color_list[-1], label=True, label_size=25,
                                           sigil="BIGARROW",
                                           name=feature.qualifiers["product"][0][0:30] + length_checker(feature),
                                           arrowhead_length=0.25, label_position="middle")

        gd_diagram.draw(format="linear", orientation="landscape", pagesize="A3", fragments=1,
                        start=int(operon[0].location.start), end=int(operon[len(operon) - 1].location.end),
                        fragment_size=0.4)
        gd_diagram.write(os.path.join(query_path, "Query" + str(index) + ".svg"), "SVG")



def query_operon_svg(as_features, outputdir):
    for index, operon in enumerate(as_features):
        query_path = os.path.join(outputdir, "query" + str(index))
        gd_diagram = Diagram("Test Diagram")
        gd_track = gd_diagram.new_track(1, name= "Query" + str(index),
                                        scale_ticks=0,
                                        greytrack=1, greytrack_labels=1, greytrack_fontsize=15)
        gd_feature_set = gd_track.new_set()
        for feature in operon:
            gd_feature_set.add_feature(feature, color=color_getter(feature), label=True, label_size=15,
                                       sigil="BIGARROW",
                                       name=feature.qualifiers["product"][0][0:20] + length_checker(feature),
                                       arrowhead_length=0.25, label_position="middle")

        gd_diagram.draw(format="linear", orientation="landscape", pagesize="A3", fragments=1,
                        start=int(operon[0].location.start), end=int(operon[len(operon) - 1].location.end),
                        fragment_size=0.6)
        gd_diagram.write(os.path.join(query_path, "Query" + str(index) + ".svg"), "SVG")


def sort_index1(x):
    return float(x[1])

def operon_maker(score_file, neighbors_file, operon_dir, query_dir, blast_file, search_range, custom_genomes=""):
    """Creates a final operon outputfile which is sorted based on hit score. Also creates .svg of the individual operon
    and a concatinated version as .svg file."""

    if custom_genomes == "":
        wd_gbff = os.path.join(operon_dir, "genome_gbff")
    else:
        wd_gbff = custom_genomes

    with open(score_file, "r") as f:
        read_tsv = csv.reader(f, delimiter="\t")
        score_res = [(row[0], row[0].split("|")[0], row[0].split("|")[1], row[1]) for row in read_tsv]


    with open(neighbors_file, "r") as f:
            read_tsv = csv.reader(f, delimiter="\t")
            corres_ids = [(row[0], row[1]) for row in read_tsv]

    # new ids of hits
    hit_ids = []
    for result in score_res:
        for id in corres_ids:
            if result[0] == id[0]: # Assembly|Number
                hit_ids.append((result[1], id[1], result[3], result[2])) #Assembly, id,  score, operon_no.

    # create list of files
    all_gbff_files = []
    for result in hit_ids:
        genome_name = result[0]
        for file in os.listdir(wd_gbff):
            if genome_name in file:
                all_gbff_files.append((os.path.join(wd_gbff, file), result[1], result[2], result[3]))
    all_gbff_files.sort(key=lambda x: float(x[2]), reverse=True) # sort results by score

    with open(os.path.join(query_dir, "operon_hits_{}.txt".format(os.path.basename(query_dir))), "w") as f:
        DELIMITER = "\t"
        # f.write("Assembly" + DELIMITER + "score" + DELIMITER + "location" + DELIMITER +  "target name"+ DELIMITER + "1" + DELIMITER + "2" +
        #         DELIMITER + "3" + DELIMITER + "4" + DELIMITER + "5" + DELIMITER + "6" + DELIMITER + "7")

        f.write("Assembly" + DELIMITER + "score" + DELIMITER + "location" + DELIMITER +  "target name"+ DELIMITER)
        for i in range(2 * search_range + 1):
            f.write(str(i+1) + DELIMITER)

    for id_tuple in all_gbff_files:
        file = id_tuple[0]
        id = [id_tuple[1]]
        score = id_tuple[2]
        operon_id = id_tuple[3]
        assembly = "_".join(os.path.basename(file).split("_")[0:2])
        fois_search, as_features, ids_duplicates = neighbors_getter_extended(id, file, search_range)
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(as_features, ids_duplicates)

        ## Get strand of id ##
        long_as_features = []
        for operon in reduced_fois_list:
            if len(operon) > 3:
                long_as_features.append(operon)
                try:
                    if id == operon[search_range].qualifiers["protein_id"][0]:
                        strand_foi = operon[search_range].location.strand
                    else:
                        for feature in operon:
                            try:
                                if id == feature.qualifiers["protein_id"]:
                                    strand_foi = feature.location.strand
                            except KeyError:
                                pass
                except KeyError:
                    for feature in operon:
                        try:
                            if id == feature.qualifiers["protein_id"]:
                                strand_foi = feature.location.strand
                        except KeyError:
                            pass
            else:
                print("Gene cluster too small, therefore it was ignored. Please check the file {}".format(file))

        if len(long_as_features) == 0:
            continue

        if strand_foi == -1:
            operon_start = long_as_features[0][0].location.start
            operon_end = long_as_features[0][-1].location.end
            reversed_as_features = []
            for operon in long_as_features:
                for feature in operon:
                    reversed_as_features.append(
                        SeqFeature(FeatureLocation(-int(feature.location.end)+operon_end, -int(feature.location.start)+operon_end,
                                    strand= -feature.location.strand), type= "CDS", qualifiers=feature.qualifiers))
            reversed_as_features.reverse()
            plus_as_features = [reversed_as_features]
        else:
            plus_as_features = long_as_features


        try:
            organism = next(SeqIO.parse(file, "genbank")).annotations["organism"]
        except:
            organism = "Unknown"

        with open(os.path.join(query_dir, "operon_hits_{}.txt".format(os.path.basename(query_dir))), "a") as f:
            DELIMITER = "\t"
            for index, operon in enumerate(long_as_features):
                f.write("\n" + assembly + DELIMITER + str(score) + DELIMITER +
                        str(int(operon[0].location.start))+"..."+str(int(operon[len(operon) - 1].location.end)) + DELIMITER + ids_duplicates[0])
                for feature in operon:
                    f.write(str(DELIMITER + feature.qualifiers["product"][0]))

        for index, operon in enumerate(plus_as_features): # as_features
            gd_diagram = Diagram("Test Diagram")
            gd_track = gd_diagram.new_track(1, name=organism +" "+assembly+" "+ids_duplicates[0]+" "+str(round(float(score),2)), scale_ticks=0,
                                            greytrack=1, greytrack_labels=1, greytrack_fontsize=20)
            gd_feature_set = gd_track.new_set()
            for pos_no, feature in enumerate(operon): # FIND NEW POSITION NUMBER!!!

                # for old_operon in as_features:
                #     for old_position, old_feature in enumerate(old_operon):
                #         if feature.qualifiers["product"] == old_feature.qualifiers["product"]: # THIS NEEDS TO GET BETTER!!!!!!!!
                #             old_pos_no = old_position
                if as_features[index] != plus_as_features[index]:
                    old_pos_no = len(as_features[index]) - 1 - pos_no
                else:
                    old_pos_no = pos_no

                gd_feature_set.add_feature(feature, color=color_getter_blastres(blast_file, assembly, operon_id, old_pos_no), label=False, label_size=25,
                                           sigil="BIGARROW",
                                           name=feature.qualifiers["product"][0][0:30] + length_checker(feature),
                                           arrowhead_length=0.25, label_position="middle")

            gd_diagram.draw(format="linear", orientation="landscape", pagesize="A3", fragments=1,
                            start=int(operon[0].location.start), end=int(operon[len(operon) - 1].location.end),
                            fragment_size=0.3)
            gd_diagram.write(os.path.join(query_dir, "Diagram_" + ids_duplicates[0] + "_" + os.path.basename(file).split(".")[0] +"_" + str(round(float(score),2)) + ".svg"), "SVG")

    # list of image files
    svgs = []
    for file in os.listdir(query_dir):
        if ".svg" in file and "Diagram_" in file:
            svgs.append(os.path.join(query_dir, file))
    svgs.sort(key=lambda x: float(".".join(x.split("_")[-1].split(".")[0:2])), reverse=True)
    try:
        query_list = []
        for file in os.listdir(query_dir):
            if "Query" in file and ".svg" in file:
                query_list.append(os.path.join(query_dir, file))
        svgs = query_list + svgs
    except:
        pass
    svg_stacker(svgs, query_dir)





def color_getter(feature):
    """Get color for feature based on product."""
    groups = ["hypothetical", "Clp", "regulator", "factor", "ase"]
    name = feature.qualifiers["product"][0]
    id = -1
    for group in groups:
        if group in name:
            id = groups.index(group)
            break
    color_list = [colors.gray,  colors.darkred, colors.purple, colors.cyan,
                  colors.magenta, colors.orange, colors.black, colors.greenyellow, colors.darkgreen]
    if id in range(len(groups)):
        color = color_list[id]
    else:
        color = colors.black
    return color

def length_checker(feature):
    if len(feature.qualifiers["product"][0]) > 20:
        return "..."
    else:
        return ""


# def get_concat_v(*args):
#     args = args
#
#     dst = Image.new('RGB', (args[0].width, args[0].height * len(args)))
#     dst.paste(args[0], (0, 0))
#
#     HEIGHT = args[0].height
#     for i in range(1, len(args)):
#         dst.paste(args[i], (0, HEIGHT))
#         HEIGHT += args[i].height
#     return dst


# def svg_stacker(svgs, outputdir):
#     BASE_WIDTH = 357 / 0.3
#     BASE_HEIGHT = 253 / 0.3
#     OFFSET = 175 / 0.3
#     opened = [svgutils.transform.fromfile(x).getroot() for x in svgs]
#     figure = svgutils.transform.SVGFigure(str(BASE_WIDTH) + "px", str(BASE_HEIGHT * len(svgs)) + "px")
#     for index, fig in enumerate(opened):
#         fig.moveto(0, index * OFFSET) # scale=0.3
#     figure.append(opened)
#     figure.save(os.path.join(outputdir, "operons_hits_svg.svg"))


##### Multiprocessing stuff #####
# def multiprocess_looper(id_tuple, operon_hits_list, blast_file, query_dir, search_range):
#     file = id_tuple[0]
#     id = [id_tuple[1]]
#     score = id_tuple[2]
#     operon_id = id_tuple[3]
#     assembly = "_".join(os.path.basename(file).split("_")[0:2])
#     fois_search, as_features, ids_duplicates = neighbors_getter_extended(id, file, search_range)
#
#     # change this to list and append at the end
#     for index, operon in enumerate(as_features):
#         general_stuff = [assembly, str(score), str(int(operon[0].location.start)) + "..."
#                          + str(int(operon[len(operon) - 1].location.end)), ids_duplicates[0]]
#         feature_list = [feature.qualifiers["product"][0] for feature in operon]
#         operon_hits_list.append(general_stuff + feature_list)
#
#     ## Get strand of id ##
#     for operon in as_features:
#         for feature in operon:
#             try:
#                 if id == feature.qualifiers["protein_id"]:
#                     strand_foi = feature.location.strand
#             except KeyError:
#                 pass
#
#     if strand_foi == -1:
#         operon_start = as_features[0][0].location.start
#         operon_end = as_features[0][-1].location.end
#         reversed_as_features = []
#         for operon in as_features:
#             for feature in operon:
#                 reversed_as_features.append(
#                     SeqFeature(FeatureLocation(-int(feature.location.end) + operon_end,
#                                                -int(feature.location.start) + operon_end,
#                                                strand=-feature.location.strand), type="CDS",
#                                qualifiers=feature.qualifiers))
#         reversed_as_features.reverse()
#         plus_as_features = [reversed_as_features]
#     else:
#         plus_as_features = as_features
#
#     try:
#         organism = next(SeqIO.parse(file, "genbank")).annotations["organism"]
#     except:
#         organism = "Unknown"
#
#     for index, operon in enumerate(plus_as_features):  # as_features
#         gd_diagram = Diagram("Test Diagram")
#         gd_track = gd_diagram.new_track(1, name=organism + " " + assembly + " " + ids_duplicates[0] + " " + str(
#             round(float(score), 2)), scale_ticks=0,
#                                         greytrack=1, greytrack_labels=1, greytrack_fontsize=20)
#         gd_feature_set = gd_track.new_set()
#         for pos_no, feature in enumerate(operon):  # FIND NEW POSITION NUMBER!!!
#
#             # for old_operon in as_features:
#             #     for old_position, old_feature in enumerate(old_operon):
#             #         if feature.qualifiers["product"] == old_feature.qualifiers["product"]: # THIS NEEDS TO GET BETTER!!!!!!!!
#             #             old_pos_no = old_position
#             if as_features[index] != plus_as_features[index]:
#                 old_pos_no = len(as_features[index]) - 1 - pos_no
#             else:
#                 old_pos_no = pos_no
#
#             gd_feature_set.add_feature(feature,
#                                        color=color_getter_blastres(blast_file, assembly, operon_id, old_pos_no),
#                                        label=False, label_size=25,
#                                        sigil="BIGARROW",
#                                        name=feature.qualifiers["product"][0][0:30] + length_checker(feature),
#                                        arrowhead_length=0.25, label_position="middle")
#
#         gd_diagram.draw(format="linear", orientation="landscape", pagesize="A3", fragments=1,
#                         start=int(operon[0].location.start), end=int(operon[len(operon) - 1].location.end),
#                         fragment_size=0.3)
#         gd_diagram.write(os.path.join(query_dir,
#                                       "Diagram_" + ids_duplicates[0] + "_" + os.path.basename(file).split(".")[
#                                           0] + "_" + str(round(float(score), 2)) + ".svg"), "SVG")
#
#
# def operon_maker(score_file, neighbors_file, operon_dir, query_dir, blast_file, search_range, custom_genomes=""):
#     """Creates a final operon outputfile which is sorted based on hit score. Also creates .svg of the individual operon
#     and a concatinated version as .svg file."""
#
#     if custom_genomes == "":
#         wd_gbff = os.path.join(operon_dir, "genome_gbff")
#     else:
#         wd_gbff = custom_genomes
#
#     with open(score_file, "r") as f:
#         read_tsv = csv.reader(f, delimiter="\t")
#         score_res = [(row[0], row[0].split("|")[0], row[0].split("|")[1], row[1]) for row in read_tsv]
#
#
#     with open(neighbors_file, "r") as f:
#             read_tsv = csv.reader(f, delimiter="\t")
#             corres_ids = [(row[0], row[1]) for row in read_tsv]
#
#     # new ids of hits
#     hit_ids = []
#     for result in score_res:
#         for id in corres_ids:
#             if result[0] == id[0]:
#                 hit_ids.append((result[1], id[1], result[3], result[2]))
#
#     # create list of files
#     all_gbff_files = []
#     for result in hit_ids:
#         genome_name = result[0]
#         for file in os.listdir(wd_gbff):
#             if genome_name in file:
#                 all_gbff_files.append((os.path.join(wd_gbff, file), result[1], result[2], result[3]))
#     all_gbff_files.sort(key=lambda x: float(x[2]), reverse=True) # sort results by score
#
#     manager = multiprocessing.Manager()
#     operon_hits_list= manager.list()
#     processes = []
#     for id_tuple in all_gbff_files:
#         arguments = [id_tuple, operon_hits_list, blast_file, query_dir, search_range]
#
#         p = multiprocessing.Process(target=multiprocess_looper, args=arguments)
#         p.start()
#         processes.append(p)
#
#     for process in processes:
#         process.join()
#
#     operon_hits_list.sort(key=sort_index1, reverse=True)
#     with open(os.path.join(query_dir, "operon_hits.txt"), "w") as f:
#         tsv_writer = csv.writer(f, delimiter="\t")
#
#         tsv_writer.writerow(["Assembly", "score", "location", "target name", "1", "2", "3", "4", "5", "6", "7"])
#         for row in operon_hits_list:
#             tsv_writer.writerow(row)
#
#
#
#     # list of image files
#     svgs = []
#     for file in os.listdir(query_dir):
#         if ".svg" in file and "Diagram_" in file:
#             svgs.append(os.path.join(query_dir, file))
#     svgs.sort(key=lambda x: float(".".join(x.split("_")[-1].split(".")[0:2])), reverse=True)
#     try:
#         query_list = []
#         for file in os.listdir(query_dir):
#             if "Query" in file and ".svg" in file:
#                 query_list.append(os.path.join(query_dir, file))
#         svgs = query_list + svgs
#     except:
#         pass
#     svg_stacker(svgs, query_dir)
