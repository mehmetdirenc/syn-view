# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import sys
from automlst_filegetter import automlst_parser, download_and_unzip
from make_database_for_blast import make_db, run_local_blast, blast_scorer_sorter2
from operon_visualization import *


def make_faa_file(gbff_file, outputdir):
    """Create a .faa file from genbank file"""
    output_name = "_".join(os.path.basename(gbff_file).split("_")[0:2]) + "_protein.faa"
    gb_records = SeqIO.parse(gbff_file, "genbank")
    print("Creating .faa file {}".format(output_name))

    unique_ids = set()
    with open(os.path.join(outputdir, output_name), "a") as f:
        for record in gb_records:
            for feature in record.features:
                if feature.type == "CDS":
                    try:
                        if feature.qualifiers["protein_id"][0] not in unique_ids:
                            record = feature_Record_converter(feature)
                            SeqIO.write(record, f, "fasta")
                            unique_ids.add(feature.qualifiers["protein_id"][0])
                    except KeyError:
                        # print("Keyerror in file")
                        continue
    return os.path.join(outputdir, output_name)

def blast_hit_ids(blast_file):
    """Parse blast file and get hit ids"""
    with open(blast_file) as tsv_file:
        read_tsv = csv.reader(tsv_file, delimiter="\t")
        hits = [row[1] for row in read_tsv]
    return hits

def run_local_blast_fmt6(query, db, output,  e_value, format=6):
    """run blastp search against custom databse."""
    cmd = ["blastp", "-db", db, "-query", query, "-out", output, "-outfmt", str(format), "-evalue", str(e_value)]
    subprocess.run(cmd, check=True)

def pipe2(automlst_file, hmm_file, automlst_parselength, query_fasta, outputdir, search_range, sorting):
    """Main pipe for downloading and scoring. Needs automlst result (mash_distances.txt)"""
    wd_faa = os.path.join(outputdir, "protein_faa")
    wd_gbff = os.path.join(outputdir, "genome_gbff")

    automlst_result = automlst_parser(automlst_file, automlst_parselength)
    download_and_unzip(automlst_result, wd_faa, wd_gbff, outputdir)

    gbff_files = os.listdir(wd_gbff)
    gbff_files.sort()
    faa_files = os.listdir(wd_faa)
    faa_files.sort()

    # Write neighbors output
    output_neighbors = "output_neighbors_reduced.txt"
    with open(os.path.join(outputdir, output_neighbors), "w") as f:
        DELIMITER = "\t"
        # f.write("operon name" + DELIMITER + "target name" + DELIMITER + "1" + DELIMITER + "2" + DELIMITER + "3" + DELIMITER + "4" + DELIMITER + "5" + DELIMITER + "6" + DELIMITER + "7")

        f.write("operon name" + DELIMITER + "target name" + DELIMITER)
        for i in range(2 * search_range + 1):
            f.write(str(i+1) + DELIMITER)

    for i in range(len(gbff_files)):
        faa_file = os.path.join(wd_faa, faa_files[i])
        gb_file = os.path.join(wd_gbff, gbff_files[i])
        # output_hmm = "output_hmm" + str(i) + "_.txt"
        # output_neighbors = "output_neighbors" + str(i) + "_.txt"
        basename = "_".join(os.path.basename(gbff_files[i]).split("_")[0:2])
        output_hmm = basename + "_hmm" +".hmm"
        fasta_output = "renamed_operons.fasta"

        outputfile_name = run_hmmsearch(hmm_file, faa_file, os.path.join(outputdir, output_hmm))
        ids = get_hit_ids(outputfile_name)

        error_files = []
        try:
            neighbors_list, fois_list, ids_duplicate = neighbors_getter_extended(ids, gb_file, search_range)
        except ValueError as e:
            print(e)
            print("\nTHE GENOME FILE WAS NOT USED FOR SCORING!!\n")
            error_files.append(gb_file)
            continue



        ### New sorting ###
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(fois_list, ids_duplicate)
        with open(os.path.join(outputdir, fasta_output), "a") as f:
            for index_operon, operon in enumerate(reduced_fois_list):
                for index_column, feature in enumerate(operon):
                    try:
                        record = feature_record_converter(feature, basename, index_operon, index_column)
                        SeqIO.write(record, f, "fasta")
                    except KeyError:
                        print("\n" + feature.qualifiers["product"][0], "has no 'translation'")

        # Convert reduced_fois_list to product list
        reduced_neighbors = []
        for operon in reduced_fois_list:
            reduced_neighbors.append([feature.qualifiers["product"][0] for feature in operon])

        with open(os.path.join(outputdir, output_neighbors), "a") as f:
            DELIMITER = "\t"
            for index, operon in enumerate(reduced_neighbors):
                f.write("\n" + basename+"|"+str(index) + DELIMITER + new_ids[index])
                for protein in operon:
                    f.write(str(DELIMITER + protein))

    if len(error_files) > 0:
        print("The follwoing files contained errors and could not be parsed:")
        print(error_files)

    fasta_file = os.path.join(outputdir, fasta_output)
    query_fasta = os.path.join(outputdir, query_fasta)
    blast_output = os.path.join(outputdir, "blast_res.xml")

    make_db(fasta_file, "local_blast_databse", outputdir)
    run_local_blast(query_fasta, os.path.join(outputdir, fasta_file), blast_output)
    blast_scorer_sorter2(blast_output, os.path.join(outputdir, "sorted.txt"), 0.04, sorting)
    operon_maker(os.path.join(outputdir, "sorted.txt"), os.path.join(outputdir, output_neighbors), outputdir, blast_output)


def pipe2_general(automlst_file, hmm_file, automlst_parselength, outputdir, search_range, download_faa=1, custom_genomes=""):
    """Main pipe for general tasks."""

    wd_faa = os.path.join(outputdir, "protein_faa")
    if custom_genomes == "":
        wd_gbff = os.path.join(outputdir, "genome_gbff")
        automlst_result = automlst_parser(automlst_file, automlst_parselength)
    else:
        wd_gbff = custom_genomes



    if download_faa == 1:
        if custom_genomes != "":
            print("Cannot download files from server if genomes are specified, please add '-download_faa 0' or remove "
                  "own genomes")
            sys.exit()
        download_and_unzip(automlst_result, wd_faa, wd_gbff, outputdir, download_faa=download_faa)

        gbff_files = os.listdir(wd_gbff)
        gbff_files.sort()
        faa_files = os.listdir(wd_faa)
        faa_files.sort()

    elif download_faa == 0:
        if custom_genomes == "":
            download_and_unzip(automlst_result, wd_faa, wd_gbff, outputdir, download_faa=download_faa)

        gbff_files = os.listdir(wd_gbff)
        gbff_files.sort()
        for file in gbff_files:
            abs_file = os.path.join(wd_gbff, file)
            faa_file_name = make_faa_file(abs_file, wd_faa)
        faa_files = os.listdir(wd_faa)
        faa_files.sort()

    # Write neighbors output
    output_neighbors = "output_neighbors_reduced.txt"
    with open(os.path.join(outputdir, output_neighbors), "w") as f:
        DELIMITER = "\t"
        # f.write("operon name" + DELIMITER + "target name" + DELIMITER + "1" + DELIMITER + "2" + DELIMITER + "3" + DELIMITER + "4" + DELIMITER + "5" + DELIMITER + "6" + DELIMITER + "7")

        f.write("operon name" + DELIMITER + "target name" + DELIMITER)
        for i in range(2 * search_range + 1):
            f.write(str(i+1) + DELIMITER)


    for i in range(len(gbff_files)):
        faa_file = os.path.join(wd_faa, faa_files[i])
        gb_file = os.path.join(wd_gbff, gbff_files[i])
        # output_hmm = "output_hmm" + str(i) + "_.txt"
        # output_neighbors = "output_neighbors" + str(i) + "_.txt"
        basename = "_".join(os.path.basename(gbff_files[i]).split("_")[0:2])
        output_hmm = basename + "_hmm" +".hmm"
        fasta_output = "renamed_operons.fasta"

        outputfile_name = run_hmmsearch(hmm_file, faa_file, os.path.join(outputdir, output_hmm))
        try:
            ids = get_hit_ids(outputfile_name)
        except UnboundLocalError:
            print("No hits found in file {}".format(gb_file))
            continue

        error_files = []
        try:
            neighbors_list, fois_list, ids_duplicate = neighbors_getter_extended(ids, gb_file, search_range)
        except ValueError as e:
            print(e)
            print("\nTHE GENOME FILE WAS NOT USED FOR SCORING!!\n")
            error_files.append(gb_file)
            continue



        ### New sorting ###
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(fois_list, ids_duplicate)
        with open(os.path.join(outputdir, fasta_output), "a") as f:
            for index_operon, operon in enumerate(reduced_fois_list):
                for index_column, feature in enumerate(operon):
                    try:
                        record = feature_record_converter(feature, basename, index_operon, index_column)
                        SeqIO.write(record, f, "fasta")
                    except KeyError:
                        print("\n", feature.qualifiers["product"][0], "has no 'translation'")

        # Convert reduced_fois_list to product list
        reduced_neighbors = []
        for operon in reduced_fois_list:
            reduced_neighbors.append([feature.qualifiers["product"][0] for feature in operon])
        with open(os.path.join(outputdir, output_neighbors), "a") as f:
            DELIMITER = "\t"
            for index, operon in enumerate(reduced_neighbors):
                f.write("\n" + basename+"|"+str(index) + DELIMITER + new_ids[index])
                for protein in operon:
                    f.write(str(DELIMITER + protein))

    if len(error_files) > 0:
        print("The follwoing files contained errors and could not be parsed:")
        print(error_files)

    fasta_file = os.path.join(outputdir, fasta_output)

    make_db(fasta_file, "local_blast_databse", outputdir)


def pipe2_general_blast(automlst_file, query_fasta, automlst_parselength, outputdir, search_range, download_faa=1,
                        custom_genomes=""):
    """Main pipe for general tasks."""
    wd_faa = os.path.join(outputdir, "protein_faa")
    if custom_genomes == "":
        wd_gbff = os.path.join(outputdir, "genome_gbff")
        automlst_result = automlst_parser(automlst_file, automlst_parselength)
    else:
        wd_gbff = custom_genomes

    if download_faa == 1:
        if custom_genomes != "":
            print("Cannot download files from server if genomes are specified, please add '-download_faa 0' or remove "
                  "own genomes")
            sys.exit()
        download_and_unzip(automlst_result, wd_faa, wd_gbff, outputdir, download_faa=download_faa)

        gbff_files = os.listdir(wd_gbff)
        gbff_files.sort()
        faa_files = os.listdir(wd_faa)
        faa_files.sort()

    elif download_faa == 0:
        if custom_genomes == "":
            download_and_unzip(automlst_result, wd_faa, wd_gbff, outputdir, download_faa=download_faa)

        gbff_files = os.listdir(wd_gbff)
        gbff_files.sort()
        for file in gbff_files:
            abs_file = os.path.join(wd_gbff, file)
            faa_file_name = make_faa_file(abs_file, wd_faa)
        faa_files = os.listdir(wd_faa)
        faa_files.sort()

    # Write neighbors output
    output_neighbors = "output_neighbors_reduced.txt"
    with open(os.path.join(outputdir, output_neighbors), "w") as f:
        DELIMITER = "\t"
        f.write("operon name" + DELIMITER + "target name" + "1" + DELIMITER + "2" + DELIMITER + "3" + DELIMITER + "4" + DELIMITER + "5" + DELIMITER + "6" + DELIMITER + "7")

    blast_output_dir = os.path.join(outputdir, "blast_databases")
    subprocess.run(["mkdir", blast_output_dir], check=True)

    for i in range(len(gbff_files)):
        faa_file = os.path.join(wd_faa, faa_files[i])
        gb_file = os.path.join(wd_gbff, gbff_files[i])
        # output_hmm = "output_hmm" + str(i) + "_.txt"
        # output_neighbors = "output_neighbors" + str(i) + "_.txt"
        basename = "_".join(os.path.basename(gbff_files[i]).split("_")[0:2])
        output_blast = basename + "_blast_res.txt"
        fasta_output = "renamed_operons.fasta"


        make_db(faa_file, faa_file, blast_output_dir)
        run_local_blast_fmt6(query_fasta, os.path.join(blast_output_dir, os.path.basename(faa_file)),
                             os.path.join(outputdir, output_blast), e_value= 1e-20)

        try:
            ids = blast_hit_ids(os.path.join(outputdir, output_blast))
        except UnboundLocalError:
            print("No hits found in file {}".format(gb_file))
            continue

        error_files = []
        try:
            neighbors_list, fois_list, ids_duplicate = neighbors_getter_extended(ids, gb_file, search_range)
        except ValueError as e:
            print(e)
            print("\nTHE GENOME FILE WAS NOT USED FOR SCORING!!\n")
            error_files.append(gb_file)
            continue



        ### New sorting ###
        reduced_fois_list, new_ids = remove_overlapping_preserve_ids(fois_list, ids_duplicate)
        with open(os.path.join(outputdir, fasta_output), "a") as f:
            for index_operon, operon in enumerate(reduced_fois_list):
                for index_column, feature in enumerate(operon):
                    try:
                        record = feature_record_converter(feature, basename, index_operon, index_column)
                        SeqIO.write(record, f, "fasta")
                    except KeyError:
                        print("\n", feature.qualifiers["product"][0], "has no 'translation'")

        # Convert reduced_fois_list to product list
        reduced_neighbors = []
        for operon in reduced_fois_list:
            reduced_neighbors.append([feature.qualifiers["product"][0] for feature in operon])

        with open(os.path.join(outputdir, output_neighbors), "a") as f:
            DELIMITER = "\t"
            for index, operon in enumerate(reduced_neighbors):
                f.write("\n" + basename+"|"+str(index) + DELIMITER + new_ids[index])
                for protein in operon:
                    f.write(str(DELIMITER + protein))

    if len(error_files) > 0:
        print("The follwoing files contained errors and could not be parsed:")
        print(error_files)

    fasta_file = os.path.join(outputdir, fasta_output)

    make_db(fasta_file, "local_blast_databse", outputdir)



def pipe2_blast_score(operon_dir, query_dir, query_fasta, sorting, custom_genomes, search_range):
    """Blasts and scores"""
    output_neighbors = "output_neighbors_reduced.txt"
    fasta_output = "renamed_operons.fasta"
    fasta_file = os.path.join(operon_dir, fasta_output)
    query_fasta = os.path.join(query_dir, query_fasta)
    blast_output = os.path.join(query_dir, "blast_res.xml")

    run_local_blast(query_fasta, os.path.join(query_dir, fasta_file), blast_output)
    blast_scorer_sorter2(blast_output, os.path.join(query_dir, "sorted.txt"), 0.04, sorting)
    operon_maker(os.path.join(query_dir, "sorted.txt"), os.path.join(operon_dir, output_neighbors),
                 operon_dir, query_dir, blast_output, search_range, custom_genomes=custom_genomes)






if __name__  == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("automlst_file", help="Specify automlst_file", action="store")
    parser.add_argument("hmm_file", help=".HMM file", action="store")
    parser.add_argument("query", help="Query fasta", action="store")
    parser.add_argument("outputdir", help="Specify absolute output directory", action="store")

    parser.add_argument("-automlst_parselength", nargs="?", default=50, type=int)
    parser.add_argument("-search_range", nargs="?", default=3, type=int)
    parser.add_argument("-sorting", help= "Define sorting: 0 -> sort with position weighting\n 1 -> use bit score", nargs="?", default=1, type=int)
    args = parser.parse_args()

    pipe2(args.automlst_file, args.hmm_file, args.automlst_parselength, args.query, args.outputdir, args.search_range, args.sorting)


