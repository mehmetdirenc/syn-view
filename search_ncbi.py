# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from Bio import Entrez
import os
import subprocess
import sys

def search_for_genome(term, outputdir, faa_foldername="protein_faa", gbff_folder_name="genome_gbff", db="assembly", retmode="gb", download_faa=0):
    """Search assembly database (ftp server) and download the .faa and .gbff file into folders."""
    Entrez.email = "A.N.Other@example.com"
    wd_faa = os.path.join(outputdir, faa_foldername)
    wd_gbff = os.path.join(outputdir, gbff_folder_name)

    if not os.path.isdir(wd_faa) or not os.path.isdir(wd_gbff):  # check if folders exist
        print("Please create folders")
        sys.exit()

    handle = Entrez.esearch(db=db, term=term, retmode=retmode) # get ids
    record = Entrez.read(handle)
    ids = record["IdList"]

    for id in ids:
        record_summary = Entrez.read(Entrez.esummary(db=db, id = id, report="full"))
        try:
            url = record_summary['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_RefSeq'] # create base url based on id
        except:
            print("ERROR FINDING URL")
            continue
        label = os.path.basename(url) # label is the file name
        link_gbff = os.path.join(url,label+'_genomic.gbff.gz')

        if download_faa == 1:
            link_faa = os.path.join(url, label + '_protein.faa.gz')
            subprocess.run(["wget", "-P", wd_faa, link_faa])

        subprocess.run(["wget", "-P", wd_gbff, link_gbff])







