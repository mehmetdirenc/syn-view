# Copyright (C) 2020 Jason Stahlecker
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of SYN-view
# SYN-view is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with SYN-view
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from Bio import SeqIO
from Bio.Seq import Seq
from neighbor_searcher2 import *
from Bio.SeqRecord import SeqRecord
import subprocess
import os


def is_overlapping(operon_feature_list1, operon_feature_list2):
    """Check if based on location the operons overlap"""
    x1 = int(operon_feature_list1[0].location.start)
    x2 = int(operon_feature_list1[-1].location.end)

    y1 = int(operon_feature_list2[0].location.start)
    y2 = int(operon_feature_list2[-1].location.end)

    if x1 <= y2 and y1 <= x2:
        return True
    else:
        return False

def remove_overlapping(list_of_operons):
    """Creates new list without overlapping operons"""
    new_fois = []
    for operon_old in list_of_operons:
        if len(new_fois) == 0:  # Append first operon regardless (Or else keyerror)
            new_fois.append(operon_old)
        else:
            found_overlapping = False
            for operon_new in new_fois:  # loop through new list and check if there is a overlapping operon
                found_overlapping = is_overlapping(operon_old, operon_new)
            if not found_overlapping:  # only append if no overlapp has been found
                new_fois.append(operon_old)
    return new_fois

def remove_overlapping_preserve_ids(list_of_operons, ids):
    """Creates new list without overlapping operons and return in other list the corresponding ids"""
    new_fois = []
    new_ids = []
    for index, operon_old in enumerate(list_of_operons):
        if len(new_fois) == 0:  # Append first operon regardless (Or else keyerror)
            new_fois.append(operon_old)
            new_ids.append(ids[index])
        else:
            found_overlapping = False
            for operon_new in new_fois:  # loop through new list and check if there is a overlapping operon
                found_overlapping = is_overlapping(operon_old, operon_new)
            if not found_overlapping:  # only append if no overlapp has been found
                new_fois.append(operon_old)
                new_ids.append(ids[index])
    return new_fois, new_ids


# create according record

def feature_record_converter(feature, basename, no_operon, no_column):
    try:
        record = SeqRecord(Seq(feature.qualifiers["translation"][0]), id=basename + "|" + str(no_operon) + "|" + str(no_column),
                           name=feature.qualifiers["protein_id"][0],
                           description=feature.qualifiers["protein_id"][0] + " " + str(feature.location.start) + "..."
                                       + str(feature.location.end) + " " + feature.qualifiers["product"][0]
                                       + " " + feature.qualifiers["gene"][0])
        return record
    except KeyError:
        try:
            record = SeqRecord(Seq(feature.qualifiers["translation"][0]),
                               id=basename + "|" + str(no_operon) + "|" + str(no_column),
                               name=feature.qualifiers["protein_id"][0],
                               description=feature.qualifiers["protein_id"][0] + " " + str(feature.location.start)
                                           + "..." + str(feature.location.end) + " " + feature.qualifiers["product"][0])
            return record
        except KeyError:
            try:
                record = SeqRecord(Seq(feature.qualifiers["translation"][0]),
                                   id=basename + "|" + str(no_operon) + "|" + str(no_column),
                                   name=feature.qualifiers["protein_id"][0],
                                   description=feature.qualifiers["protein_id"][0] + " " +str(feature.location.start)
                                               + "..." + str(feature.location.end))
                return record
            except KeyError:
                try:
                    record = SeqRecord(Seq(feature.qualifiers["translation"][0]),
                                       id=basename + "|" + str(no_operon) + "|" + str(no_column),
                                       name="UNKNOWN",
                                       description="UNKNOWN" + " " +
                                                   str(feature.location.start) + "..." + str(feature.location.end))
                    return record
                except KeyError:
                    print("\nFEATURE WITH KEYERROR FOUND!\n", basename, str(feature.location.start) + "..." + str(feature.location.end))
                    raise KeyError
        # record = SeqRecord(Seq(""), id=basename + "|" + str(no_operon) + "|" + str(no_column),
        #                    name="Location: "+ str(feature.location.start) + "..." + str(feature.location.end),
        #                    description="Location: "+ str(feature.location.start) + "..." + str(feature.location.end))


def feature_Record_converter(feature):
    try:
        record = SeqRecord(Seq(feature.qualifiers["translation"][0]), id=feature.qualifiers["protein_id"][0], name=feature.qualifiers["product"][0], description=feature.qualifiers["product"][0])
        return record
    except KeyError:
        try:
            record = SeqRecord(Seq(feature.qualifiers["translation"][0]), id=feature.qualifiers["protein_id"][0],
                               name=feature.qualifiers["protein_id"][0], description=feature.qualifiers["protein_id"][0])
            return record
        except KeyError:
            raise KeyError
